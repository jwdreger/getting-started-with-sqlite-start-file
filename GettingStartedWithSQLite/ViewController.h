//
//  ViewController.h
//  GettingStartedWithSQLite
//
//  Created by James Dreger on 3/10/14.
//  Copyright (c) 2014 James Dreger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UITextField *access;
@property (weak, nonatomic) IBOutlet UILabel *status;

- (IBAction)save:(id)sender;
- (IBAction)checkUser:(id)sender;


@end
