//
//  main.m
//  GettingStartedWithSQLite
//
//  Created by James Dreger on 3/10/14.
//  Copyright (c) 2014 James Dreger. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
